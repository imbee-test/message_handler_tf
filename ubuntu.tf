module "ubuntu" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "ubuntu"

  ami                    = "ami-052efd3df9dad4825"
  instance_type          = "t3.micro"
  key_name               = aws_key_pair.deployer.key_name
  monitoring             = false
  vpc_security_group_ids = [ aws_security_group.ubuntu_sg.id ]
  subnet_id              = "${element(module.vpc.public_subnets, 0)}"
  private_ip             = var.ubuntu_ip
  associate_public_ip_address = true

  ######IMPORTANT#######
  # Just for simplicity, we expose the db username and password here
  # We can create the whole mysql server in ansible with ansible-vault

  user_data = <<-EOF
                #!/bin/bash
                apt update && apt install -y mysql-server
                sed -i -e 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
                mysql --execute="CREATE DATABASE queue;"
                mysql --execute="CREATE USER 'queue_user'@'%' IDENTIFIED BY 'password';"
                mysql --execute="GRANT ALL PRIVILEGES ON queue.* TO 'queue_user'@'%';"
                mysql --execute="flush privileges;"
                systemctl restart mysql
                EOF
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

output "ubuntu_instance_ip" {
  description = "The public ip for ssh access"
  value       = module.ubuntu.public_ip
}

resource "aws_key_pair" "deployer" {
  key_name   = "deploy-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_security_group" "ubuntu_sg" {
  name        = "ubuntu_sg"
  vpc_id      = module.vpc.vpc_id


    ingress {
    cidr_blocks      = [ "0.0.0.0/0", ]
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    }
    ingress {
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }
    
    egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    }

  tags = {
    Name = "allow_ubuntu"
  }
}