resource "aws_sqs_queue" "terraform_queue" {
  name                        = "notification_fcm.fifo"
  fifo_queue                  = true
  content_based_deduplication = true
}

output "queue_url" {
  description = "queue url"
  value       = aws_sqs_queue.terraform_queue.id
}